# frozen_string_literal: true

class License
  def initialize(expiry:)
    @expiry = expiry
  end

  def valid?
    Time.now <= @expiry
  end

  def self.[](type)
    case type
    when :valid then new(expiry: (Time.now + (60 * 60 * 24 * 365 * 10)))
    when :expired then new(expiry: (Time.now - (60 * 60 * 24)))
    end
  end
end
